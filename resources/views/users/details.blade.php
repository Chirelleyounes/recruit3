@extends('layouts.app')

@section('title', 'Edit user')

@section('content')
    <h1>User details</h1>
    <form method="post" action="{{ route('users.update', $user->id) }}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label>User name:</label>
            <h5>{{ $user->name }}</h5>
        </div>
        <div class="form-group">
            <label>User email</label>
            <h5>{{ $user->email }}</h5>
        </div>
        <div class="form-group">
            <label>User Roles</label>
            @foreach ($user->roles as $role)
                <h5>{{ $role->name }}</h5>
            @endforeach
        </div>
        <div class="form-group">
            <label>Created at</label>
            <h5> {{ $user->created_at }}</h5>
        </div>
        <div class="form-group">
            @if (\Auth::check() && \Auth::user()->isAdmin())
                <label for="department_id">Department</label>
                <div class="col-md-3">
                    <select class="form-control" name="department_id">
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}" @if ($user->department_id == $department->id) selected
                        @endif>
                        {{ $department->name }}
                        </option>
            @endforeach
            </select>
        </div>
        </div>
        <div>
            <input type="submit" name="submit" value="Update Department">
        </div>
        @if (Session::has('notchangeable'))
            <div class='alert alert-danger'>
                {{ Session::get('notchangeable') }}
            </div>
        @endif
    @else
        <div class="form-group">
            <label>Department</label>
            <h5>{{ $user->department->name }}
            </h5>
            @endif
        </div>
    </form>
    </body>

    </html>
@endsection
