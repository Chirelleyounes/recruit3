@extends('layouts.app')

@section('title', 'Users')

@section('content')
    @if (Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{ Session::get('notallowed') }}
        </div>
    @endif
    @if (Session::has('managerchange'))
        <div class='alert alert-danger'>
            {{ Session::get('managerchange') }}
        </div>
    @endif
    <h1>List of users</h1>
    <table class="table table-dark">
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Roles</th>
            <th>Department</th>
            <th>Details</th>
            <th>Delete</th>
        </tr>
        <!-- the table data -->
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <select class="form-control">
                        @foreach ($user->roles as $role)
                            <option>{{ $role->name }}</option>
                        @endforeach
                    </select>
                </td>
                <td>{{ $user->department->name }}</td>
                <td>
                    <a href="{{ route('users.details', $user->id) }}">Details</a>
                </td>
                <td>
                    @if (!$user->isManager())
                        <a href="{{ route('users.manager.switch', $user->id) }}">Make Manager</a>
                    @else
                        <a href="{{ route('users.manager.switch', $user->id) }}">Remove Manager Role</a>

                    @endif
                </td>
                <td><a href="{{ route('users.delete', $user->id) }}">Delete User</a></td>
            </tr>
        @endforeach
    </table>
@endsection
