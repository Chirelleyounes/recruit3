<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Role;
use \App\Department;
use \App\UserRole;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function details($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.details', compact('user', 'departments'));
    }

    public function changeManager($id)
    {
        Gate::authorize('is-admin');
        $user = User::findOrFail($id);
        $manager_role = Role::where('name', 'manager')->first()->id;

        if ($user->isManager()) {
            UserRole::where('user_id', $id)->where('role_id', $manager_role)->first()->delete();
            Session::flash('managerchange', 'Manager Role has been removed.');
        } else {
            $role = new UserRole();
            $role->user_id = $id;
            $role->role_id = $manager_role;
            $role->save();
            Session::flash('managerchange', 'Manager Role has been added.');
        }
        return redirect()->route('users.index');
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($user->candidates->count() > 0) {
            Session::flash('notchangeable', 'You cannot change the user department because he owns a candidate.');
        }


        $user->department_id = $request->department_id;
        $user->save();
        return redirect()->route('users.details', ['id' => $id]);
    }

    public function delete($id)
    {
        Gate::authorize('is-admin');
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index');
    }
}
